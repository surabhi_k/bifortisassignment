<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bifortis Assignment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>  
 
  <!-- Inline css -->
  <style>
  html, body {
        background-color:  #BC8F8F;

        height: 100%;
    }
    .fill-height {
        min-height: 100%;
        height:auto !important;
        height: 100%;
        padding-top: 200px;
    }
  </style>

</head>
<body>
	<div class="container">
 	 <form class="form-inline text-center" role="form" action="" method="POST">
 	   <div class="form1">
 	   <h2><b>Weather Check!</b></h2> 
 	   </div>
 	   </br>
		<select name="cityVal" style="width: 300px">
		  <option value="">Select city...</option>
		  <option value="Mumbai">Mumbai</option>
		  <option value="Bangalore">Bangalore</option>
		  <option value="Pune">Pune</option>
		  <option value="Chennai">Chennai</option>
		  <option value="Hyderabad">Hyderabad</option>
		</select>
		</br>
		</br>
   	    <button type="submit" class="btn btn-default" name="submit">Submit</button>
   	    </div>
    </form>
    <br/>
        <div style="margin-left: 43%;">
        <?php
		{
			if(isset($_POST['submit'])){
			$city = $_POST['cityVal'];
			$output = getWeather($city);
			$high_Temp = $output['query']['results']['channel']['item']['forecast'][0]['high'];
			$low_Temp = $output['query']['results']['channel']['item']['forecast'][0]['low'];
			$date_Temp = $output['query']['results']['channel']['item']['forecast'][0]['date'];
			$text_Temp = $output['query']['results']['channel']['item']['forecast'][0]['text'];

			echo "<b>".$city.", ".$date_Temp."</b>"."<br>"."<b>Maximum Temperature: </b>".$high_Temp . "&deg;F" . "<br>"."<b>Minimum Temperature: </b>".$low_Temp . "&deg;F";
			echo  "<br><b>"."'".$text_Temp."'"."</b><br>";
			}
		}
		function getWeather ($city) {
		    $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20%28select%20woeid%20from%20geo.places%281%29%20where%20text=%22" . $city . "%22%29&format=json";    
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $output=curl_exec($ch);
		    curl_close($ch);
		    $output1=json_decode($output,true);
		    return $output1;
	   }
	?>
	</div>
   </div>
</body>
</html>

